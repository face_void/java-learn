package com.javacourse.se.lesson5;

import com.javacourse.se.lesson15.Constructor;
import com.javacourse.se.lesson7.Bus;

public class Main {
    public static void main(String[] args) {
        Constructor constructor = new Constructor();
        constructor.foo();
    }
}
