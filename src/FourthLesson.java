import java.util.Random;
import java.util.Scanner;

public class FourthLesson {

    public static char[][] map;
    public static final int SIZE = 3;

    public static final char DOT_EMPTY = '.';
    public static final char DOT_X = 'X';
    public static final char DOT_O = 'O';

    public static void main(String[] args) {
        // Крестики - нолики.
        // 1. Инициализация поля.
        initMap();
        // 2. Вывод поля.
        printMap();

        // 3. Цикл.
        while (true) {
            // Итерация:

            // 3.1. Ход игрока.
            humanTurn();
            // - вывод поля
            printMap();
            //      3.2. Проверка победы (продолжение или выход).
            if (isWinner(DOT_X)) {
                System.out.println("Победил человек!");
                break;
            }
            //      3.3. Проверка ничьи (продолжение или выход).
            if (isMapFull()) {
                System.out.println("Ничья!");
                break;
            }
            // 3.4. Ход компьютера.
            aiTurn();
            // - вывод поля
            printMap();
            //      3.5. Проверка победы (продолжение или выход).
            if (isWinner(DOT_O)) {
                System.out.println("T-1000 победил!");
                break;
            }
            //      3.6. Проверка ничьи (продолжение или выход).
            if (isMapFull()) {
                System.out.println("Ничья!");
                break;
            }
        }

        System.out.println("Игра закончена!");
    }

    // 1. Инициализация поля.
    public static void initMap() {
        map = new char[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                map[i][j] = DOT_EMPTY;
            }
        }
    }

    // 2. Вывод поля.
    public static void printMap() {
        for (int i = 0; i <= SIZE; i++) {
            System.out.print(i + " ");
        }

        System.out.println();

        for (int i = 0; i < SIZE; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < SIZE; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }

    // 3.1. Ход игрока.
    public static void humanTurn() {
        Scanner scanner = new Scanner(System.in);

        int x;
        int y;

        do {
            System.out.println("Введите координаты в формате X Y");
            x = scanner.nextInt() - 1; // 1,2,3 -> 0,1,2
            y = scanner.nextInt() - 1;
        } while (isCellInvalid(x, y));

        map[y][x] = DOT_X;
    }

    // 3.4. Ход компьютера.
    public static void aiTurn() {
        Random random = new Random();

        int x;
        int y;

        do {
            x = random.nextInt(SIZE);
            y = random.nextInt(SIZE);
        } while (isCellInvalid(x, y));

        System.out.println("Компьютер походил в точку " + (x + 1) + " " + (y + 1));
        map[y][x] = DOT_O;
    }

    public static boolean isCellInvalid(int x, int y) {
        if (x < 0 || x >= SIZE || y < 0 || y >= SIZE) {
            return true;
        }

        if (map[y][x] == DOT_EMPTY) {
            return false;
        }

        return true;
    }

    // 3.2. Проверка победы (продолжение или выход).
    public static boolean isWinner(char symb) {
        if (map[0][0] == symb && map[0][1] == symb && map[0][2] == symb) {
            return true;
        }
        if (map[1][0] == symb && map[1][1] == symb && map[1][2] == symb) {
            return true;
        }
        if (map[2][0] == symb && map[2][1] == symb && map[2][2] == symb) {
            return true;
        }

        if (map[0][0] == symb && map[1][0] == symb && map[2][0] == symb) {
            return true;
        }
        if (map[0][1] == symb && map[1][1] == symb && map[2][1] == symb) {
            return true;
        }
        if (map[0][2] == symb && map[1][2] == symb && map[2][2] == symb) {
            return true;
        }

        if (map[0][0] == symb && map[1][1] == symb && map[2][2] == symb) {
            return true;
        }
        if (map[0][2] == symb && map[1][1] == symb && map[2][0] == symb) {
            return true;
        }

        return false;
    }
    // 3.3. Проверка ничьи (продолжение или выход).
    public static boolean isMapFull() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (map[i][j] == DOT_EMPTY) {
                    return false;
                }
            }
        }

        return true;
    }
}
