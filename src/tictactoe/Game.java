package tictactoe;

public class Game {
    public static char[][] map;
    public static char playerSide;

    public static final int MAP_SIZE = 3;

    public static final char SYMBOL_X = 'X';
    public static final char SYMBOL_0 = '0';
    public static final char SYMBOL_EMPTY = '.';

    public static final AI AI = new AI();

    public static void main(String[] args) {
        // 1. Спросим игрока, каким символом он будет играть
        System.out.println("Выберите, кем вы будете играть и введите с клавиатуры " + SYMBOL_X + " (английская X) или " + SYMBOL_0 + " (цифра ноль)");
        while (!isCorrectPlayerSide()) {
            setPlayerSide(askPlayerSide());
        }
        // 2. Отображаем карту
        // 3. Первые ходят Х (игрок или компьютер)
        //  3.1 Проверяем победа или ничья (возможно конец игры)
        //  3.2 Отображаем карту
        // 4. Ходят 0
        //  4.1 Проверяем победа или ничья (возможно конец игры)
        //  4.2 Отображаем карту
    }

    public static boolean isCorrectPlayerSide() {
        return playerSide == SYMBOL_X || playerSide == SYMBOL_0;
    }

    public static void setPlayerSide(char side) {
        playerSide = side;
    }

    public static char askPlayerSide() {

    }

    private static void initMap() {

    }
}
